============================================================

Quickstart:
    https://gitlab.com/garantiertnicht/DiscordEmbed

Please, submit Bugs and Suggestions here
    https://gitlab.com/garantiertnicht/DiscordEmbed/issues

Developers Discord:
    https://discord.gg/jgmfBMk

============================================================

This is free software, intended to help you. I do not
provide any form of warranty. For more information,
See LICENSE