/*
 * DiscordEmbed, a programm that allows the creation of rich embeds
 * Copyright (C) 2017  garantiertnicht
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ADDITIONAL TERMS as in Section 7:
 *     * Any modified version must be marked as such in `%info`.
 */

package de.garantiertnicht.DiscordEmbed;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import com.sun.istack.internal.Nullable;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed.Field;

/**
 * This class guards against empty strings and provides sensible defaults.
 */
public class EmbedBuilder {
    private String title = null;
    private String description = null;
    private String pretext = null;
    private String link = null;
    private String author = null;
    private String authorLink = null;
    private String authorImage = null;
    private String footer = null;
    private String footerImage = null;
    private String inlineImage = null;
    private String belowImage = null;
    private List<Field> fields = new LinkedList<>();
    private Color color = null;

    @Nullable
    private String nullifyEmptyString(String string) {
        if (string == null || string.isEmpty()) {
            return null;
        } else {
            try {
                return string.trim();
            } catch (IllegalStateException e) {
                return null;
            }
        }
    }

    public EmbedBuilder setTitle(@Nullable String title) {
        this.title = nullifyEmptyString(title);
        return this;
    }

    public EmbedBuilder setPretext(@Nullable String pretext) {
        this.pretext = nullifyEmptyString(pretext);
        return this;
    }

    public EmbedBuilder setDescription(@Nullable String description) {
        this.description = nullifyEmptyString(description);
        return this;
    }

    public EmbedBuilder setLink(@Nullable String link) {
        this.link = nullifyEmptyString(link);
        return this;
    }

    public EmbedBuilder setAuthor(@Nullable String author) {
        this.author = nullifyEmptyString(author);
        return this;
    }

    public EmbedBuilder setAuthorLink(@Nullable String authorLink) {
        this.authorLink = nullifyEmptyString(authorLink);
        return this;
    }

    public EmbedBuilder setAuthorImage(@Nullable String url) {
        this.authorImage = nullifyEmptyString(url);
        return this;
    }

    public EmbedBuilder setFooter(@Nullable String footer) {
        this.footer = nullifyEmptyString(footer);
        return this;
    }

    public EmbedBuilder setFooterImage(@Nullable String url) {
        this.footerImage = nullifyEmptyString(url);
        return this;
    }

    public EmbedBuilder setInlineImage(@Nullable String url) {
        this.inlineImage = nullifyEmptyString(url);
        return this;
    }

    public EmbedBuilder setBelowImage(@Nullable String url) {
        this.belowImage = nullifyEmptyString(url);
        return this;
    }

    public EmbedBuilder addField(String title, String value, boolean inline) {
        fields.add(new Field(title, value, inline));
        return this;
    }

    public EmbedBuilder setFieldsFromProperties(String newlineSeparatedProperties) {
        if(newlineSeparatedProperties != null && !newlineSeparatedProperties.isEmpty()) {
            fields = new LinkedList<>();

            Scanner scanner = new Scanner(newlineSeparatedProperties + '\n');
            while(scanner.hasNextLine()) {
                String[] field = scanner.nextLine().split("=", 2);

                if(field.length != 2) {
                    throw new IllegalArgumentException("Illegal Field Format");
                }

                boolean inline = field[1].endsWith(" inline");

                if(inline) {
                    field[1] = field[1].substring(0, field[1].length() - " inline".length());
                }

                fields.add(new Field(field[0], field[1].replace('|', '\n'), inline));
            }

            scanner.close();
        }

        return this;
    }

    public EmbedBuilder setColor(@Nullable Color color) {
        this.color = color;
        return this;
    }

    public Message build() {

        MessageBuilder messageBuilder = new MessageBuilder();

        if(title != null || description != null || footer != null || author != null || inlineImage != null || belowImage != null || !fields.isEmpty()) {
            net.dv8tion.jda.core.EmbedBuilder builder = new net.dv8tion.jda.core.EmbedBuilder();

            builder.setColor(color);

            builder.setTitle(title, link);
            builder.setDescription(description);
            builder.setFooter(footer, footerImage);

            String authorUrl = authorLink == null ? link : authorLink;
            builder.setAuthor(author, authorUrl, authorImage);

            builder.setThumbnail(inlineImage);
            builder.setImage(belowImage);

            fields.forEach(builder::addField);

            messageBuilder.setEmbed(builder.build());
        }

        if(pretext != null) {
            messageBuilder.append(pretext);
        }

        return messageBuilder.build();
    }
}
