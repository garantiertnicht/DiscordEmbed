/*
 * DiscordEmbed, a programm that allows the creation of rich embeds
 * Copyright (C) 2017  garantiertnicht
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ADDITIONAL TERMS as in Section 7:
 *     * Any modified version must be marked as such in `%info`.
 */

package de.garantiertnicht.DiscordEmbed.autocomplete;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import com.sun.istack.internal.NotNull;
import de.garantiertnicht.DiscordEmbed.autocomplete.AutocompleteHandler.AutocompletionResult;

/**
 * This class is used to autocomplete Strings.
 */
public final class Autocomplete<T> {
    /**
     * Creates a builder for an Autocomplete class.
     */
    public static class Builder<T> {
        private Set<AutocompleteHandler<T>> autocompleteHandlers = new HashSet<>(1);

        /**
         * Add a handler to the list. Every handler should only exist once.
         * @param autocompleteHandler The handler to add
         */
        public Builder add(AutocompleteHandler<T> autocompleteHandler) {
            autocompleteHandlers.add(autocompleteHandler);
            return this;
        }

        /**
         * Builds the Autocompleter.
         * @return The built object
         */
        public Autocomplete<T> build() {
            return new Autocomplete<>(new CopyOnWriteArraySet<>(autocompleteHandlers));
        }
    }

    private final CopyOnWriteArraySet<AutocompleteHandler<T>> autocompleteHandlers;

    public Autocomplete(CopyOnWriteArraySet<AutocompleteHandler<T>> autocompleteHandlers) {
        this.autocompleteHandlers = autocompleteHandlers;
    }

    /**
     * Completes the given text.
     * @param text The text to autocomplete
     * @param courserPosition The current position of the cuorsor
     * @return The autocompleted text, the same if no completion occurred.
     */
    public String autocomplete(@NotNull String text, int courserPosition, @NotNull T object) {
        if(text.length() < 4) {
            return text;
        }

        int startPos = courserPosition - 2;
        int endPos = courserPosition - 2;

        for(int i = Math.max(courserPosition - 2, 0); i >= 0; i--) {
            if(Character.isWhitespace(text.charAt(i))) {
                break;
            }

            startPos--;
        }

        for(int i = Math.max(courserPosition - 2, 0); i < text.length(); i++) {
            if(Character.isWhitespace(text.charAt(i))) {
                break;
            }

            endPos++;
        }

        startPos++;

        if(endPos < 0 || startPos < 0 || startPos > endPos) {
            return text;
        }

        String word = text.substring(startPos, endPos);

        if(word.length() < 4) {
            return text;
        }

        String result = null;

        for(AutocompleteHandler autocompleteHandler : autocompleteHandlers) {
            AutocompletionResult autocompletionResult = autocompleteHandler.getResult(word, object);

            // Did the handler found a duplicate?
            if(autocompletionResult == AutocompleteHandler.ResultDuplicate) {
                return text;
            }

            // Check for duplicate result
            if(result != null && autocompletionResult != AutocompleteHandler.ResultEmpty) {
                return text;
            }

            if(autocompletionResult != AutocompleteHandler.ResultEmpty) {
                result = autocompletionResult.result;
            }
        }

        if(result != null) {
            StringBuilder newText = new StringBuilder();
            newText.append(text.substring(0, startPos));
            newText.append(result);

            if(text.length() > endPos) {
                newText.append(text.substring(endPos + 1, text.length()));
            }

            return newText.toString();
        }

        return text;
    }
}
