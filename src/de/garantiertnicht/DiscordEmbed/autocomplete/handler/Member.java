/*
 * DiscordEmbed, a programm that allows the creation of rich embeds
 * Copyright (C) 2017  garantiertnicht
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ADDITIONAL TERMS as in Section 7:
 *     * Any modified version must be marked as such in `%info`.
 */

package de.garantiertnicht.DiscordEmbed.autocomplete.handler;

import de.garantiertnicht.DiscordEmbed.autocomplete.AutocompleteHandler;
import net.dv8tion.jda.core.entities.Guild;


public class Member extends AutocompleteHandler<Guild> {
    public static final AutocompleteHandler<Guild> instance = new Member();

    private Member() {}

    @Override
    public AutocompletionResult getResult(String word, Guild guild) {
        if(!word.startsWith("@")) {
            return AutocompleteHandler.ResultEmpty;
        }

        word = word.substring(1);
        String result = null;

        for(net.dv8tion.jda.core.entities.Member member : guild.getMembers()) {
            if((member.getNickname() != null && member.getNickname().startsWith(word)) || member.getUser().getName().startsWith(word)) {
                if(result == null) {
                    result = member.getAsMention();
                } else {
                    return AutocompleteHandler.ResultDuplicate;
                }
            }
        }

        if(result == null) {
            return AutocompleteHandler.ResultEmpty;
        } else {
            return AutocompleteHandler.getResultWithObject(result);
        }
    }
}
