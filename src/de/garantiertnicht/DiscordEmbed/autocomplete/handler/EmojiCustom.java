/*
 * DiscordEmbed, a programm that allows the creation of rich embeds
 * Copyright (C) 2017  garantiertnicht
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ADDITIONAL TERMS as in Section 7:
 *     * Any modified version must be marked as such in `%info`.
 */

package de.garantiertnicht.DiscordEmbed.autocomplete.handler;

import de.garantiertnicht.DiscordEmbed.Main;
import de.garantiertnicht.DiscordEmbed.autocomplete.AutocompleteHandler;
import net.dv8tion.jda.core.entities.Emote;
import net.dv8tion.jda.core.entities.Guild;

public class EmojiCustom extends AutocompleteHandler<Guild> {
    public static final AutocompleteHandler<Guild> instance = new EmojiCustom();

    private EmojiCustom() {}

    @Override
    public AutocompletionResult getResult(String word, Guild guild) {
        if(!word.startsWith(":")) {
            return AutocompleteHandler.ResultEmpty;
        }

        word = word.substring(1);

        boolean exactMatch = false;
        if(word.endsWith(":")) {
            word = word.substring(0, word.length() - 1);
            exactMatch = true;
        }

        Emote result = null;
        for(Emote emote : Main.getInstance().getClient().getEmotes()) {
            if(emote == null || emote.getName() == null || !emote.canInteract(guild.getSelfMember())) {
                continue;
            }

            if((exactMatch && !emote.getName().equals(word)) || !emote.getName().startsWith(word)) {
                continue;
            }

            if(result != null) {
                return AutocompleteHandler.ResultDuplicate;
            } else {
                result = emote;
            }
        }

        return result == null ? AutocompleteHandler.ResultEmpty : AutocompleteHandler.getResultWithObject(result.getAsMention());
    }
}
