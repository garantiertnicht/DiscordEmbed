/*
 * DiscordEmbed, a programm that allows the creation of rich embeds
 * Copyright (C) 2017  garantiertnicht
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ADDITIONAL TERMS as in Section 7:
 *     * Any modified version must be marked as such in `%info`.
 */

package de.garantiertnicht.DiscordEmbed.autocomplete.handler;

import com.vdurmont.emoji.Emoji;
import com.vdurmont.emoji.EmojiManager;
import de.garantiertnicht.DiscordEmbed.autocomplete.AutocompleteHandler;
import net.dv8tion.jda.core.entities.Guild;

public class EmojiStandard extends AutocompleteHandler<Guild> {
    public static final AutocompleteHandler<Guild> instance = new EmojiStandard();

    private EmojiStandard() {};

    @Override
    public AutocompletionResult getResult(String word, Guild guild) {
        if(!word.startsWith(":")) {
            return AutocompleteHandler.ResultEmpty;
        }

        word = word.substring(1, word.length());
        Emoji result = null;

        for(Emoji emote : EmojiManager.getAll()) {
            boolean match = false;

            for (String name : emote.getAliases()) {
                if (name.startsWith(word)) {
                    match = true;
                    break;
                }
            }

            if(!match) {
                continue;
            }

            if(result != null) {
                return AutocompleteHandler.ResultDuplicate;
            } else {
                result = emote;
            }
            break;
        }

        return result == null ? AutocompleteHandler.ResultEmpty : AutocompleteHandler.getResultWithObject(result.getUnicode());
    }
}
