/*
 * DiscordEmbed, a programm that allows the creation of rich embeds
 * Copyright (C) 2017  garantiertnicht
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ADDITIONAL TERMS as in Section 7:
 *     * Any modified version must be marked as such in `%info`.
 */

package de.garantiertnicht.DiscordEmbed.autocomplete;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

public abstract class AutocompleteHandler<T> {
    /**
     * Gets a result indicating we have more then one solution
     */
    public static final AutocompletionResult ResultDuplicate = new AutocompletionResult(null);

    /**
     * Gets a result indicating there was no result
     */
    public static final AutocompletionResult ResultEmpty = null;

    /**
     * This is a class used to return results.
     */
    public static class AutocompletionResult {
        /**
         * The result. Is null if a duplication occurred.
         */
        @Nullable
        public final String result;

        /**
         * Sets the result.
         * @param result The result. May be null if the result is a duplicate.
         */
        private AutocompletionResult(@Nullable String result) {
            this.result = result;
        }
    }

    /**
     * Gets a successful result.
     * @param result The result to return
     * @return The warped result
     */
    public static AutocompletionResult getResultWithObject(@NotNull String result) {
        return new AutocompletionResult(result);
    }

    /**
     * Autocomplete a word.
     * @param word The word to autocomplete
     * @param object The guild correctly selected
     * @return See {@link AutocompletionResult}
     */
    @NotNull
    public abstract AutocompletionResult getResult(@NotNull String word, @NotNull T object);
}
