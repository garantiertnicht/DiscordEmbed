package de.garantiertnicht.DiscordEmbed;

import com.sun.javafx.collections.ObservableListWrapper;
import de.garantiertnicht.DiscordAutoCredidentials.discord.ClientToken;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.GridPane;
import net.dv8tion.jda.core.AccountType;

import java.net.URL;
import java.util.*;

public class AutoLogin implements Initializable {
    private final List<ClientToken> availableTokens;
    public ChoiceBox<String> tokens;
    public Button button;
    public GridPane pane;

    public AutoLogin() {
        this.availableTokens = Main.getInstance().getTokens();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loadTokens();

        if(Main.initialUpdateResult != null) {
            UpdateChecker.addUpdate(pane, Main.initialUpdateResult);
        } else if(Math.random() < 0.05) {
            UpdateChecker.addLink(pane, "I have a Discord Server btw ;)", "https://discord.gg/jgmfBMk");
        }
    }

    public void login(ActionEvent actionEvent) {
        int selectedIndex = tokens.getSelectionModel().getSelectedIndex();
        if(selectedIndex >= availableTokens.size()) {
            Main.getInstance().show("login", "Login");
        } else {
            Main.LoginResponse response = Main.getInstance().loginToken(AccountType.CLIENT, availableTokens.get(selectedIndex).token());

            switch (response) {
                case INVALID_TOKEN:
                    new Alert(Alert.AlertType.ERROR, "This Login is no longer valid.", ButtonType.CLOSE).show();
                    break;
                case REQUEST_FAILED:
                    new Alert(Alert.AlertType.ERROR, "Something went wrong. Please retry later :-)", ButtonType.CLOSE).show();
                    break;
                case SUCCESS:
                    button.setDisable(true);
                    button.setText("Please wait, we are loading…");
            }
        }
    }

    private void loadTokens() {
        LinkedList<String> tokenList = new LinkedList<>();

        for(ClientToken token : availableTokens) {
            tokenList.add(token.discordClient().name());
        }

        tokenList.add("Manual Login");

        tokens.setItems(new ObservableListWrapper<>(tokenList));
        tokens.getSelectionModel().select(0);
    }
}
