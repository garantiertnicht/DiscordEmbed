/*
 * DiscordEmbed, a programm that allows the creation of rich embeds
 * Copyright (C) 2017  garantiertnicht
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ADDITIONAL TERMS as in Section 7:
 *     * Any modified version must be marked as such in `%info`.
 */

package de.garantiertnicht.DiscordEmbed;

import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class UpdateAgent implements Runnable {
    private boolean banned = false;
    private String updatedVersion = null;

    private final String userid;
    private static final HttpClient client = new DefaultHttpClient();

    public boolean isBanned() {
        return banned;
    }

    public String getUpdatedVersion() {
        return updatedVersion;
    }

    public UpdateAgent(String userid) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hashed = digest.digest(userid.getBytes());

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < hashed.length; ++i) {
            stringBuilder.append(Integer.toHexString((hashed[i] & 0xFF) | 0x100).substring(1, 3));
        }

        this.userid = stringBuilder.toString();
    }

    @Override
    public void run() {
        while (!Thread.interrupted()) {
            refresh();

            try {
                Thread.sleep(3_600_000); // One Hour
            } catch (InterruptedException e) {
                return;
            }

            updatedVersion = UpdateChecker.getUpdatedVersion();
        }
    }

    public void refresh() {
        HttpGet request = new HttpGet("https://gitlab.com/snippets/1655964/raw");

        try {
            HttpResponse response = UpdateAgent.client.execute(request);
            try (InputStream inputStream = response.getEntity().getContent()) {
                Scanner scanner = new Scanner(inputStream);

                while (scanner.hasNextLine()) {
                    if (scanner.next().equals(userid)) {
                        banned = true;
                    }
                }

                scanner.close();
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
