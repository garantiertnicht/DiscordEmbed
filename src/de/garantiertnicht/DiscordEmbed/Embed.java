/*
 * DiscordEmbed, a programm that allows the creation of rich embeds
 * Copyright (C) 2017  garantiertnicht
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ADDITIONAL TERMS as in Section 7:
 *     * Any modified version must be marked as such in `%info`.
 */

package de.garantiertnicht.DiscordEmbed;

import java.awt.Color;
import java.net.URL;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

import com.sun.javafx.collections.ObservableListWrapper;
import de.garantiertnicht.DiscordEmbed.autocomplete.Autocomplete;
import de.garantiertnicht.DiscordEmbed.autocomplete.handler.EmojiCustom;
import de.garantiertnicht.DiscordEmbed.autocomplete.handler.EmojiStandard;
import de.garantiertnicht.DiscordEmbed.autocomplete.handler.Everyone;
import de.garantiertnicht.DiscordEmbed.autocomplete.handler.Group;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import net.dv8tion.jda.core.JDA.Status;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.*;


public class Embed implements Initializable {
    public ChoiceBox<String> guild;
    public ChoiceBox<String> channel;
    public TextField pretext;
    public TextField title;
    public TextArea description;
    public TextField footer;
    public ColorPicker color;
    public Button sendButton;
    public TextField link;
    public TextField footerImage;
    public TextArea fields;
    public TextField authorName;
    public TextField authorImage;
    public TextField authorLink;
    public TextField imageInline;
    public TextField imageBelow;
    public GridPane pane;

    private LinkedList<Guild> guildList = new LinkedList<>();
    private LinkedList<MessageChannel> channelList = new LinkedList<>();
    private int selectedGuild = -1;
    private int selectedChannel = -1;

    private boolean updateAdded = false;
    private long noSendBefore = 0;

    public static final Autocomplete<Guild> autocompleteDiscord = new Autocomplete.Builder<Guild>()
        .add(de.garantiertnicht.DiscordEmbed.autocomplete.handler.Channel.instance)
        .add(Everyone.instance)
        .add(Group.instance)
        .add(de.garantiertnicht.DiscordEmbed.autocomplete.handler.Member.instance)
        .add(EmojiStandard.instance)
        .add(EmojiCustom.instance)
        .build();

    public static final Autocomplete<Guild> autocompleteEmojiCompatible = new Autocomplete.Builder<Guild>()
        .add(EmojiStandard.instance)
        .add(EmojiCustom.instance)
        .build();

    public static final Autocomplete<Guild> autocompleteStandartReplaces = new Autocomplete.Builder<Guild>()
        .add(EmojiStandard.instance)
        .build();

    public void initialize(URL url, ResourceBundle resourceBundle) {
        color.setValue(javafx.scene.paint.Color.GRAY);

        guild.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            selectedGuild = newValue.intValue();
            setChannels(selectedGuild);

            selectedChannel = -1;
        });

        channel.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            selectedChannel = newValue.intValue();
        });

        addTextChangedListener(pretext, autocompleteDiscord);
        addTextChangedListener(description, autocompleteDiscord);
        addTextChangedListener(fields, autocompleteDiscord);

        addTextChangedListener(title, autocompleteEmojiCompatible);

        addTextChangedListener(authorName, autocompleteStandartReplaces);
        addTextChangedListener(footer, autocompleteStandartReplaces);

        color.setValue(javafx.scene.paint.Color.BLACK);

        setGuilds();
    }

    public void addTextChangedListener(TextInputControl control, Autocomplete<Guild> autocomplete) {
        control.textProperty().addListener((observable, oldValue, newValue) -> autocomplete(control, autocomplete));
    }

    public void refresh(ActionEvent actionEvent) {
        Main.getInstance().getUpdateAgent().refresh();
        setGuilds();
    }

    public void send(ActionEvent actionEvent) {
        if(Main.getInstance().getUpdateAgent() == null || Main.getInstance().getUpdateAgent().isBanned()) {
            Main.getInstance().openBrowser("https://www.youtube.com/watch?v=FXPKJUE86d0");
            sendButton.setDisable(true);
            sendButton.setText("Have a nice day :-)");
            return;
        }

        if(selectedChannel == -1) {
            channel.setStyle("-fx-border-color: red;");
            return;
        } else {
            channel.setStyle("");
        }

        javafx.scene.paint.Color selectedColor = color.getValue();

        if(System.currentTimeMillis() < noSendBefore) {
            new Alert(AlertType.WARNING, "Please, do not spam.").showAndWait();
            noSendBefore = System.currentTimeMillis() + 10_000;
            return;
        }

        noSendBefore = System.currentTimeMillis() + 1500;

        try {

            Message message = new EmbedBuilder().
                setPretext(pretext.getText())
                .setTitle(title.getText())
                .setDescription(description.getText())
                .setFooter(footer.getText())
                .setFooterImage(footerImage.getText())
                .setAuthor(authorName.getText())
                .setAuthorImage(authorImage.getText())
                .setAuthorLink(authorLink.getText())
                .setLink(link.getText())
                .setColor(new Color((int) (selectedColor.getRed() * 255), (int) (selectedColor.getGreen() * 255), (int) (selectedColor.getBlue() * 255)))
                .setInlineImage(imageInline.getText())
                .setBelowImage(imageBelow.getText())
                .setFieldsFromProperties(fields.getText())
                .build();

            MessageChannel messageChannel = channelList.get(selectedChannel);

            channelList.get(selectedChannel).sendMessage(message).queue((ignored) -> {}, (throwable) -> new Alert(AlertType.ERROR, throwable.toString(), ButtonType.OK).show());
        } catch (Exception exception) {
            Alert alert = new Alert(AlertType.ERROR, "Oh, sorry about that. Press ok if you want to automagically refresh.\n" + exception.toString(),
                ButtonType.OK, ButtonType.CANCEL);

            alert.showAndWait().ifPresent((buttonType -> {
                if(buttonType == ButtonType.OK) {
                    setGuilds();
                }
            }));
        }

        if(!updateAdded) {
            // The delay for not running in in a separate thread should also help to prevent spam.
            if(Main.getInstance().getUpdateAgent() == null || Main.getInstance().getUpdateAgent().getUpdatedVersion() != null) {
                UpdateChecker.addUpdate(pane, Main.getInstance().getUpdateAgent().getUpdatedVersion());
            }
        }
    }

    private void setGuilds() {
        LinkedList<String> guilds = new LinkedList<>();
        guildList.clear();

        List<Guild> guildsVisible = new LinkedList<>(Main.getInstance().getClient().getGuilds());
        guildsVisible.sort(Comparator.comparing(o -> o.getName().toLowerCase()));

        guildList.add(null);
        guilds.add("Direct Messages");

        for(Guild guild : guildsVisible) {
            guilds.add(guild.getName());
            guildList.add(guild);
        }

        guild.setItems(new ObservableListWrapper<>(guilds));
        selectedGuild = -1;
    }

    private void setChannels(int newValue) {
        if(newValue < 0 || newValue >= guildList.size()) {
            return;
        }

        LinkedList<String> channels = new LinkedList<>();
        channelList.clear();

        if(newValue != 0) {
            Guild guild = guildList.get(newValue);
            List<TextChannel> channelsInGuild = new LinkedList<>(guild.getTextChannels());
            channelsInGuild.sort(Comparator.comparing(Channel::getPosition));

            for(TextChannel channel : channelsInGuild) {
                if(guild.getMember(Main.getInstance().getClient().getSelfUser()).hasPermission(channel, Permission.MESSAGE_READ, Permission.MESSAGE_WRITE, Permission.MESSAGE_EMBED_LINKS)) {
                    channels.add(channel.getName());
                    channelList.add(channel);
                }
            }
        } else {
            List<PrivateChannel> channelsToAdd = new LinkedList<>(Main.getInstance().getClient().getPrivateChannels());
            channelsToAdd.sort(Comparator.comparing(MessageChannel::getName));

            for(PrivateChannel channel : channelsToAdd) {
                channels.add(channel.getName());
                channelList.add(channel);
            }
        }

        if(channelList.isEmpty()) {
            if(Main.getInstance().getClient().getStatus() != Status.CONNECTED) {
                channel.setDisable(true);
                LinkedList<String> elements = new LinkedList<>();
                elements.add("Please wait…");
                channel.setItems(new ObservableListWrapper<>(elements));
                channel.getSelectionModel().select(0);

                new Thread(() -> {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        // ignored
                    }

                    if(channel.getSelectionModel().getSelectedIndex() == newValue) {
                        Platform.runLater(() -> setChannels(newValue));
                    }
                }).start();

                return;
            } else {
                channel.setDisable(true);
            }
        } else {
            channel.setDisable(false);
            channel.setValue(null);
        }

        channel.setItems(new ObservableListWrapper<>(channels));
    }

    private void autocomplete(TextInputControl control, Autocomplete<Guild> autocomplete) {
        if(selectedGuild > 0) {
            control.setText(autocomplete.autocomplete(control.getText(), control.getCaretPosition(), guildList.get(selectedGuild)));
        } else if(selectedGuild == 0) {
            control.setText(autocompleteStandartReplaces.autocomplete(control.getText(), control.getCaretPosition(), guildList.get(selectedGuild)));
        }
    }

    public void openFieldsWiki(ActionEvent actionEvent) {
        Main.getInstance().openBrowser("https://gitlab.com/garantiertnicht/DiscordEmbed/wikis/Fields");
    }
}
