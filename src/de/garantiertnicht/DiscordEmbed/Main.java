/*
 * DiscordEmbed, a programm that allows the creation of rich embeds
 * Copyright (C) 2017  garantiertnicht
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ADDITIONAL TERMS as in Section 7:
 *     * Any modified version must be marked as such in `%info`.
 */

package de.garantiertnicht.DiscordEmbed;

import javax.security.auth.login.LoginException;
import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.List;

import com.github.zafarkhaja.semver.Version;
import de.garantiertnicht.DiscordAutoCredidentials.discord.ClientToken;
import de.garantiertnicht.DiscordAutoCredidentials.discord.DiscordClientRegistry;
import javafx.application.Application;
import javafx.application.HostServices;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.exceptions.RateLimitedException;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import scala.collection.JavaConverters;

@SuppressWarnings("Singleton")
public class Main extends Application {
    public static final Version VERSION = Version.valueOf("0.6.1");
    public static final ReleaseKind VERSION_KIND = UpdateChecker.getReleaseKindFor(VERSION.getPreReleaseVersion());
    public static final String initialUpdateResult = UpdateChecker.getUpdatedVersion();

    private static Main INSTANCE;
    private JDA client;
    private Stage stage;
    private UpdateAgent updateAgent = null;

    private final HostServices hostServices = this.getHostServices();
    private List<ClientToken> tokens;

    @Override
    public void start(Stage primaryStage) throws Exception{
        INSTANCE = this;
        stage = primaryStage;

        tokens = JavaConverters.seqAsJavaList(DiscordClientRegistry.clientTokens(DiscordClientRegistry.clientTokens$default$1()));

        if(tokens.size() != 0) {
            show("AutoLogin", "Login");
        } else {
            show("login", "Login");
        }
        primaryStage.show();
    }

    List<ClientToken> getTokens() {
        List<ClientToken> list = tokens;
        tokens = null;

        return list;
    }

    public void show(String resource, String name) {
        stage.setOnCloseRequest((WindowEvent) -> System.exit(0));

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource(resource + ".fxml"));
        } catch (IOException e) {
            System.err.println("Can't load window " + resource);
            e.printStackTrace();
            System.exit(1);
        }
        stage.setScene(new Scene(root));
        stage.setTitle(name);
    }

    enum LoginResponse {SUCCESS, REQUEST_FAILED, INVALID_MAIL, INVALID_PASSWORD, INVALID_TOKEN};

    public LoginResponse loginClient(String email, String password) {
        if(email.isEmpty()) {
            return LoginResponse.INVALID_MAIL;
        } else if(password.isEmpty()) {
            return LoginResponse.INVALID_PASSWORD;
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("email", email);
        jsonObject.put("password", password);

        HttpClient client = new DefaultHttpClient();
        HttpPost request = new HttpPost("https://discordapp.com/api/auth/login");
        request.setEntity(new StringEntity(jsonObject.toString(), ContentType.APPLICATION_JSON));

        try {
            HttpResponse response = client.execute(request);
            try (InputStream inputStream = response.getEntity().getContent()) {
                Scanner scanner = new Scanner(inputStream);
                StringBuilder responseString = new StringBuilder();

                while (scanner.hasNextLine()) {
                    responseString.append(scanner.nextLine());
                }

                scanner.close();

                JSONObject responseObject = new JSONObject(responseString.toString());

                if (responseObject.has("email")) {
                    return LoginResponse.INVALID_MAIL;
                } else if (responseObject.has("password")) {
                    return LoginResponse.INVALID_PASSWORD;
                } else {
                    if (responseObject.has("token")) {
                        return loginToken(AccountType.CLIENT, responseObject.getString("token"));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return LoginResponse.REQUEST_FAILED;
        }

        return LoginResponse.REQUEST_FAILED;
    }

    public LoginResponse loginToken(AccountType type, String token) {
        ReadyEvent readyEvent = new ReadyEvent();

        try {
            this.client = new JDABuilder(type).setToken(token).setStatus(OnlineStatus.INVISIBLE).addEventListener(readyEvent).buildAsync();
        } catch (LoginException e) {
            e.printStackTrace();
            return LoginResponse.INVALID_TOKEN;
        } catch (RateLimitedException e) {
            e.printStackTrace();
            return LoginResponse.REQUEST_FAILED;
        }

        return LoginResponse.SUCCESS;
    }

    public static Main getInstance() {
        return INSTANCE;
    }

    public JDA getClient() {
        return client;
    }

    public UpdateAgent getUpdateAgent() {
        return updateAgent;
    }

    public void openBrowser(String url) {
        try {
            hostServices.showDocument(url);
        } catch (NullPointerException exception) { // This seems to be the only way to check
            ButtonType COPY_URL_BUTTON = new ButtonType("Copy Link", ButtonBar.ButtonData.YES);

            Alert alert = new Alert(Alert.AlertType.INFORMATION, url, COPY_URL_BUTTON, ButtonType.CLOSE);
            alert.setHeaderText("Failed to open browser");
            Optional<ButtonType> result = alert.showAndWait();
            if(result.isPresent() && result.get() == COPY_URL_BUTTON) {
                StringSelection urlSelection = new StringSelection(url);
                Toolkit.getDefaultToolkit().getSystemClipboard().setContents(urlSelection, null);
            }
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

    private class ReadyEvent extends ListenerAdapter {
        private long lastInfoCommandIssued = 0;

        private ReadyEvent() {}

        @Override
        public void onReady(net.dv8tion.jda.core.events.ReadyEvent event) {
            super.onReady(event);

            event.getJDA().getPrivateChannels();
            event.getJDA().getPresence().setPresence(OnlineStatus.INVISIBLE, true);

            try {
                updateAgent = new UpdateAgent(client.getSelfUser().getId());
            } catch (NoSuchAlgorithmException ignored) {}

            new Thread(updateAgent).start();

            Platform.runLater(() -> show("embed", "DiscordEmbed"));
        }

        @Override
        public void onMessageReceived(MessageReceivedEvent event) {
            if(!event.getAuthor().getId().equals(client.getSelfUser().getId())) {
                return;
            }

            if(updateAgent == null || updateAgent.isBanned()) {
                return;
            }

            if(event.getMessage().getRawContent().startsWith("%info")) {
                long time = System.currentTimeMillis();

                if(time - lastInfoCommandIssued < 10_000) {
                    lastInfoCommandIssued = time;
                    event.getMessage().editMessage("⚠️ Do not spam!").queue();

                    new Thread(() -> {
                        try {
                            Thread.sleep(2500);
                        } catch (InterruptedException e) {
                            return;
                        }

                        event.getMessage().delete().queue();
                    }).start();

                    return;
                } else {
                    lastInfoCommandIssued = time;
                }


                Color color = new Color((int) (Math.random() * 256), (int) (Math.random() * 256), (int) (Math.random() * 256));

                String version = "Running DiscordEmbed version " + Main.VERSION;

                EmbedBuilder builder = new EmbedBuilder()
                    .setTitle("So you want to create colorful boxes?")
                    .setDescription("With DiscordEmbed, a Free Open Source tool, you can create messages like this one!")
                    .addField("Download Link", "https://gitlab.com/garantiertnicht/DiscordEmbed/tags", true)
                    .addField("Developers Discord", "https://discord.gg/jgmfBMk", true)
                    .addField("Licensing Information", "This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by\n" + "    the Free Software Foundation, either version 3 of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details: https://www.gnu.org/licenses/agpl-3.0.txt", false)
                    .addField("Additional Terms", "All modified versions must be marked as modified in the `%info` command.", false)
                    .setFooter(version)
                    .setColor(color);

                event.getMessage().editMessage(builder.build()).queue();

                String latestVersion = UpdateChecker.getUpdatedVersion();

                if(latestVersion != null) {
                    version += " (New update available: " + latestVersion + ")";
                    builder.setFooter(version);

                    event.getMessage().editMessage(builder.build()).queue();
                }

                return;
            } else if(!event.getMessage().getRawContent().startsWith("%embed")) {
                return;
            }

            String defaultId = event.getChannel().getId();

            if(event.getChannelType() == ChannelType.PRIVATE) {
                defaultId = "@" + ((PrivateChannel) event.getChannel()).getUser().getId();
            }

            if(event.getMessage().getRawContent().equals("%embed")) {
                event.getMessage().editMessage("```%embed\n" +
                    "channel=" + defaultId + "\n" +
                    "pretext=Text before Message|Multiline Supported\n" +
                    "title=Title\n" +
                    "description=A description|with multiline support\n" +
                    "r=255\n" +
                    "g=0\n" +
                    "b=0\n" +
                    "imageInline=https://url.to/image\n" +
                    "imageBelow=https://utl.to/image\n" +
                    "footer=Text in the footer" +
                    "\nfooterImage=https://url.to/image\n" +
                    "author=" + event.getAuthor().getName() + "\n" +
                    "authorImage=https://url.to/image\n" +
                    "authorLink=https://example.edu/\n" +
                    "link=https://example.edu/\n" +
                    "fields=Some=Fields inline;;As in=the normal format inline;;As in the textbox=from the|GUI\n```" +
                    "~ DiscordEmbed, a free open-source tool to create embeds. Check out <https://gitlab.com/garantiertnicht/DiscordEmbed/>!").queue();
            } else {
                try {
                    Properties properties = new Properties();
                    properties.load(new StringReader(event.getMessage().getContent().split("\n", 2)[1]));

                    int r = Integer.valueOf(properties.getProperty("r", "0"));
                    int g = Integer.valueOf(properties.getProperty("g", "0"));
                    int b = Integer.valueOf(properties.getProperty("b", "0"));

                    MessageChannel channel;
                    String channelId = properties.getProperty("channel", defaultId);
                    boolean isPrivate = false;

                    if(channelId.startsWith("@")) {
                        channelId = channelId.substring(1);
                        channel = client.getUserById(channelId).openPrivateChannel().complete();
                        isPrivate = true;
                    } else {
                        channel = client.getTextChannelById(channelId);
                    }

                    for(Object keyObject : properties.keySet()) {
                        String key = (String) keyObject;
                        String string = properties.getProperty(key);
                        boolean replacedSomething = false;

                        if(!isPrivate) {
                            Guild guild = ((TextChannel) channel).getGuild();

                            while (true) {
                                int index = string.indexOf("[@]");

                                if (index == -1) {
                                    break;
                                } else {
                                    replacedSomething = true;
                                    string = string.replace("[@]", "");

                                    string = Embed.autocompleteDiscord.autocomplete(string, index + 2, guild);
                                }
                            }
                        }

                        if(replacedSomething) {
                            properties.setProperty(key, string);
                        }
                    }

                    Message message = new EmbedBuilder()
                        .setPretext(properties.getProperty("pretext", "").replace('|', '\n'))
                        .setTitle(properties.getProperty("title", ""))
                        .setDescription(properties.getProperty("description", "").replace('|', '\n'))
                        .setAuthor(properties.getProperty("author", null))
                        .setAuthorImage(properties.getProperty("authorImage", null))
                        .setAuthorLink(properties.getProperty("authorLink", null))
                        .setFooter(properties.getProperty("footer", null))
                        .setFooterImage(properties.getProperty("footerImage"))
                        .setInlineImage(properties.getProperty("imageInline", null))
                        .setBelowImage(properties.getProperty("imageBelow", null))
                        .setColor(new Color(r, g, b))
                        .setFieldsFromProperties(properties.getProperty("fields", "").replace(";;", "\n"))
                        .setLink(properties.getProperty("link", null))
                        .build();

                    channel.sendMessage(message).queue((ignored) -> {}, (throwable) -> event.getChannel().sendMessage(throwable.toString()).queue());
                } catch (Exception e) {
                    e.printStackTrace();
                    event.getChannel().sendMessage("```" + e.toString() + "```").queue();
                }
            }
        }
    }
}
