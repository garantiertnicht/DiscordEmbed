/*
 * DiscordEmbed, a programm that allows the creation of rich embeds
 * Copyright (C) 2017  garantiertnicht
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ADDITIONAL TERMS as in Section 7:
 *     * Any modified version must be marked as such in `%info`.
 */

package de.garantiertnicht.DiscordEmbed;

import java.net.URL;
import java.util.ResourceBundle;

import de.garantiertnicht.DiscordEmbed.Main.LoginResponse;
import javafx.collections.FXCollections;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import net.dv8tion.jda.core.AccountType;

public class Login implements Initializable {

    public TextField email;
    public PasswordField password;
    public Button loginButton;
    public ChoiceBox tokenSelect;
    public PasswordField token;
    public GridPane pane;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tokenSelect.setItems(FXCollections.observableArrayList(AccountType.values()));
        tokenSelect.getSelectionModel().select(AccountType.CLIENT);

        if(Main.initialUpdateResult != null) {
            UpdateChecker.addUpdate(pane, Main.initialUpdateResult);
        } else if(Math.random() < 0.05) {
            UpdateChecker.addLink(pane, "I have a Discord Server btw ;)", "https://discord.gg/jgmfBMk");
        }
    }

    public void login() {
        loginButton.setDisable(true);

        String mail = email.getText();
        String pass = password.getText();

        LoginResponse response = LoginResponse.REQUEST_FAILED;

        if(token.getLength() != 0) {
            response = Main.getInstance().loginToken((AccountType) tokenSelect.getValue(), token.getText());
        } else {
            response = Main.getInstance().loginClient(mail, pass);
        }

        email.setStyle("");
        password.setStyle("");
        token.setStyle("");

        switch (response) {
            case INVALID_MAIL:
                email.selectAll();
                email.requestFocus();
                email.setStyle("-fx-border-color: red;");
                break;
            case INVALID_PASSWORD:
                password.selectAll();
                password.requestFocus();
                password.setStyle("-fx-border-color: red;");
                break;
            case REQUEST_FAILED:
                Alert alert = new Alert(AlertType.ERROR, "Login failed. Please try again later.", ButtonType.OK);
                alert.show();
                break;
            case INVALID_TOKEN:
                token.selectAll();
                token.requestFocus();
                token.setStyle("-fx-border-color: red;");
                break;
            case SUCCESS:
                loginButton.setText("Loading stuff...");
        }

        loginButton.setDisable(response == LoginResponse.SUCCESS);
    }
}
