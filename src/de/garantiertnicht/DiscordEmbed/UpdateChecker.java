/*
 * DiscordEmbed, a programm that allows the creation of rich embeds
 * Copyright (C) 2017  garantiertnicht
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ADDITIONAL TERMS as in Section 7:
 *     * Any modified version must be marked as such in `%info`.
 */

package de.garantiertnicht.DiscordEmbed;

import java.io.InputStream;
import java.util.Scanner;

import com.github.zafarkhaja.semver.Version;
import javafx.geometry.HPos;
import javafx.scene.control.Hyperlink;
import javafx.scene.layout.GridPane;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

public class UpdateChecker {

    public static String getUpdatedVersion() {
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet("https://gitlab.com/api/v4/projects/2734790/repository/tags");

        try {
            HttpResponse response = client.execute(request);
            try (InputStream inputStream = response.getEntity().getContent()) {
                Scanner scanner = new Scanner(inputStream);
                StringBuilder responseString = new StringBuilder();

                while (scanner.hasNextLine()) {
                    responseString.append(scanner.nextLine());
                }

                scanner.close();

                JSONArray array = new JSONArray(responseString.toString());
                for(int i = 0; i < array.length(); i++) {
                    JSONObject tag = array.getJSONObject(i);

                    String versionString = tag.getString("name");
                    Version latestVersion = Version.valueOf(versionString);
                    ReleaseKind kind = getReleaseKindFor(latestVersion.getPreReleaseVersion());

                    if (latestVersion.greaterThan(Main.VERSION) && kind.ordinal() <= Main.VERSION_KIND.ordinal()) {
                        return versionString;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void addUpdate(GridPane pane, String version) {
        addLink(pane, "New update available!", "https://gitlab.com/garantiertnicht/DiscordEmbed/tags/" + version);
    }

    public static void addLink(GridPane pane, String text, String link) {
        Hyperlink hyperlink = new Hyperlink(text);
        hyperlink.setOnAction((event) -> {
            Main.getInstance().openBrowser(link);
        });

        pane.add(hyperlink, 0, pane.getRowConstraints().size(), 2, 1);
        GridPane.setHalignment(hyperlink, HPos.CENTER);
    }

    public static ReleaseKind getReleaseKindFor(String preReleaseString) {
        ReleaseKind kind = ReleaseKind.RELEASE;

        if (preReleaseString.startsWith("rc")) {
            kind = ReleaseKind.RC;
        } else if (preReleaseString.startsWith("indev")) {
            kind = ReleaseKind.INDEV;
        }

        return kind;
    }
}

enum ReleaseKind {
    RELEASE, RC, INDEV
}