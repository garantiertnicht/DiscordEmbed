# Welcome to DiscordEmbed!
DiscordEmbed is a free open-source tool to create Rich Embeds.

## What are Embeds?
Embeds are also known as Link Previews. This app allows you to create such
fancy boxes for its client!

## Logging in
In order to use the tool, you first need to log in.
You can either choose to use:
* Email/Password
* Tokens (Booth Client and Bot tokens are supported)

Your data will not be stored on disk or transited to anything else than Discord.

## Using the GUI
Using the GUI is simple. Just fill in the fields needed (title and description
are required) and hit send! The only complicated thing may be fields, but
there is a
[handy little wiki page](https://gitlab.com/garantiertnicht/DiscordEmbed/wikis/Fields)
explaining it.

Also note: In some fields, mentions are automagically completed to the internal
Discord syntax after 3 matching characters and a unique match.

## Using the chat listener
If you type `%embed`, the message will be edited to contain a template.
Simply replace the part behind the `=` to edit the values. In `pretext`,
`description` and field values you may use `|` for a new line, in `fields`
you must use `;;` character where a new line would be needed in the GUI
Text-Box.

## Caveats and known issues
* **Users who don't want to see link previews won't see embeds!**
* Video embeds are not supported.
* Starting might take long.
* Mobile Users may not see mentions being formatted.

## Suggestions and support
Please send in any suggestions and errors to the
[Issue Tracker](https://gitlab.com/garantiertnicht/DiscordEmbed/issues).
If you need quick support, hop into my
[Discord Server](https://discord.gg/jgmfBMk).

## License
This project is using AGPL (science 0.5.3-indev). Additional Term as under section 7: Any modified version must be marked as such in the `%info` command.